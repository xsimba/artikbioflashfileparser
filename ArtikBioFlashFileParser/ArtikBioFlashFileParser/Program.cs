﻿using System;
using System.IO;
using System.Linq;
using System.Text;

namespace ArtikBioFlashFileParser
{
    class Program
    {

        //Stream Parsing
        byte[] packet_array;
        int packet_array_idx; //packet_array index 
        enum pack_states { FIND_0X7E, FIND_ESCAPE_CHAR };
        pack_states pack_state;
        private String[] FileNames;
        private String FileTimeStamp;
        private String DownloadPath;
        private FileStream[] FilesHandle;
        private FileStream[] SeqFilesHandle;
        private UInt32[] DatalossCount = { 0, 0 };
        private FileStream RawFileHandle;
        private BinaryWriter RawDataWriter;
        private BinaryReader RawDataReader;
        private int MaxStreamNumber;

        System.IO.FileStream open_stream_files(int stream_type)
        {
            String FullFileName;
            String FullPathFile;
            System.IO.FileStream fs;

            // create directory
            if (!System.IO.Directory.Exists(DownloadPath))
            {
                System.IO.Directory.CreateDirectory(DownloadPath);
                log_status("Directory:" + DownloadPath + " Was Created!");
            }


            // build full data file name + path
            FullFileName = FileNames[stream_type] + FileTimeStamp + ".csv";
            FullPathFile = System.IO.Path.Combine(DownloadPath, FullFileName);
            if (!System.IO.File.Exists(FullPathFile))
            {
                using (fs = System.IO.File.Create(FullPathFile))
                {
                    log_status("File:" + FullPathFile + " Was Created!");
                    return fs;
                }
            }
            else
            {
                log_status("File:" + FullPathFile + " Already Exists ...");
                return null;
            }

        }

        System.IO.FileStream open_seq_files(int stream_type)
        {
            String FullFileName;
            String FullPathFile;
            System.IO.FileStream fs;

            // create directory
            if (!System.IO.Directory.Exists(DownloadPath))
            {
                System.IO.Directory.CreateDirectory(DownloadPath);
                log_status("Directory:" + DownloadPath + " Was Created!");
            }


            // build full seq file name + path
            FullFileName = "Seq_" + stream_type.ToString() + "_" + FileTimeStamp + ".csv";
            FullPathFile = System.IO.Path.Combine(DownloadPath, FullFileName);
            if (!System.IO.File.Exists(FullPathFile))
            {
                using (fs = System.IO.File.Create(FullPathFile))
                {
                    log_status("File:" + FullPathFile + " Was Created!");
                    return fs;
                }
            }
            else
            {
                log_status("File:" + FullPathFile + " Already Exists ...");
                return null;
            }

        }


        void parse_stream_data(byte[] stream_packet, int packet_length)
        {
            // Protocol header is 5 bytes, then packet header start by the follwing structure:
            //typedef struct _Stream_Header_t
            //{
            //    uint32_t m_streamtype;    --> offset 5    bytes
            //    uint32_t m_sequencecount; --> offset 9   bytes 
            //    uint32_t m_timestamp;     --> offset 13   bytes
            //    uint32_t m_timestampend;  --> offset 17   bytes
            //    uint32_t m_samplecount;   --> offset 21   bytes
            //}
            //StreamHeader_t;

            UInt32 SampleCount, i, SeqNum;
            double TimeStamp;
            UInt64 TimeStampLong;
            float OutPutVal;
            int StreamType;
            UInt16 CalculatedFCS = 0;
            UInt16 RcvdFCS = 0;
            byte PacketType = (byte)(stream_packet[1] >> 4); //get protocol packet type 
            byte[] floatArray = new byte[4];

            //make sure this is a data packet 
            if (PacketType != 1 || packet_length < 10)
                return;

            StreamType = (int)(stream_packet[8] << 24 | stream_packet[7] << 16 | stream_packet[6] << 8 | stream_packet[5]);
            SeqNum = (UInt32)(stream_packet[12] << 24 | stream_packet[11] << 16 | stream_packet[10] << 8 | stream_packet[9]);
            // since revision 0.12.0 timestamp is double
            TimeStamp = BitConverter.ToDouble(stream_packet, 13);
            TimeStampLong = (UInt64)(TimeStamp * 1000.0);
            SampleCount = (UInt32)(stream_packet[24] << 24 | stream_packet[23] << 16 | stream_packet[22] << 8 | stream_packet[21]);
            RcvdFCS = (UInt16)(stream_packet[packet_length - 1] << 8 | stream_packet[packet_length - 2]);

            // do not collect for uneeded stream types 
            if (StreamType >= MaxStreamNumber)
            {
                return; // get next packet
            }

            // Calculate frame checksum
            for (i = 1; i < packet_length - 2; i++)
            {
                CalculatedFCS += stream_packet[i];
            }

            // if the FCS do not match - exit and drop frame
            if (CalculatedFCS != RcvdFCS)
            {
                log_status("FCS Error! Packet Type:" + StreamType.ToString());
                DatalossCount[0]++;
                return;
            }


            // Open files for this stream
            if (FilesHandle[StreamType] == null)
            {
                FilesHandle[StreamType] = open_stream_files(StreamType);
                SeqFilesHandle[StreamType] = open_seq_files(StreamType);

            }

            var CSV = new StringBuilder();

            // get sample and save to file
            for (i = 0; i < (SampleCount * 4); i += 4)
            {
                /* convert to float */
                floatArray[0] = stream_packet[25 + i];
                floatArray[1] = stream_packet[26 + i];
                floatArray[2] = stream_packet[27 + i];
                floatArray[3] = stream_packet[28 + i];
                OutPutVal = BitConverter.ToSingle(floatArray, 0);

                var newLine = string.Format("{0},{1},", TimeStampLong.ToString(), OutPutVal.ToString());
                CSV.AppendLine(newLine);
            }

            File.AppendAllText(FilesHandle[StreamType].Name, CSV.ToString());

            // get seq number and save to file
            String Seq_newLine = string.Format("{0},{1}\r\n", SeqNum.ToString(), TimeStampLong.ToString());

            File.AppendAllText(SeqFilesHandle[StreamType].Name, Seq_newLine);

        }

        private UInt64[] sort_seq_files(FileStream fs)
        {

            try
            {
                UInt32 i;
                UInt64[] Values = null;

                log_status("Sorting file Seq number:");

                if (fs != null)
                {
                    log_status(fs.Name + ".....");
                    string[] lines = File.ReadAllLines(fs.Name);
                    Values = new UInt64[lines.Length];

                    for (i = 0; i < lines.Length; i++)
                    {
                        Values[i] = Convert.ToUInt64(lines[i].Trim(','));
                    }

                    System.Array.Sort(Values);

                }
                log_status("done!");
                return Values;
            }
            catch (System.IO.IOException e)
            {
                log_status("Error Message = "+e.Message);
                return null;
            }
        }

        private void sort_files(FileStream fs)
        {
            try
            {
                log_status("Sorting file Data:");

                if (fs != null)
                {
                    log_status(fs.Name + ".....");
                    string[] lines = File.ReadAllLines(fs.Name);
                    var data = lines;
                    var sorted = data.Select(line => new
                    {
                        SortKey = UInt64.Parse(line.Split(',')[0]),
                        Line = line
                    })
                                    .OrderBy(x => x.SortKey)
                                    .Select(x => x.Line);
                    File.WriteAllLines(fs.Name + ".sorted.csv", sorted);
                }
                log_status("done!");
            }
            catch (System.IO.IOException e)
            {
                log_status("Error Message = "+ e.Message);
            }
        }



        private void parse_binary_file(String FileToParse)
        {
            String FileName = "";

            if (FileToParse != "")
            {
                FileName = FileToParse;
            }
            else
            {
                FileName = RawFileHandle.Name;
            }

            using (RawDataReader = new BinaryReader(File.Open(FileName, FileMode.Open)))
            {
                {

                    // Position and length variables.
                    long pos = 0;
                    long percent = 0;
                    long percent_shadow = 0;

                    // Use BaseStream.
                    long length = (long)(RawDataReader.BaseStream.Length);

                    while (pos < length)
                    {
                        percent = Convert.ToInt64((Convert.ToDouble(pos) / Convert.ToDouble(length)) * 100.0);
                        if ((percent - percent_shadow) >= 1)
                        {
                            percent_shadow = percent;
                            log_Parse((int)percent);
                        }


                        // Read byte
                        byte b = RawDataReader.ReadByte();
                        switch (pack_state)
                        {
                            case pack_states.FIND_0X7E:  // looking for first 0x7e
                                if (b == 0x7e)
                                {
                                    packet_array_idx = 0;
                                    packet_array[packet_array_idx] = 0x7e;
                                    pack_state = pack_states.FIND_ESCAPE_CHAR;
                                    packet_array_idx++;
                                }

                                break;

                            case pack_states.FIND_ESCAPE_CHAR:
                                if (b == 0x7e) // found second 0x7e - close packet
                                {
                                    packet_array[packet_array_idx] = 0x7e;

                                    if (packet_array_idx > 2) // this is a real packet
                                    {
                                        //send to next stage
                                        parse_stream_data(packet_array, packet_array_idx);

                                    }
                                    pack_state = pack_states.FIND_0X7E;
                                    packet_array_idx = 0;
                                }
                                else if (b == 0x7d)  //found stuffing char
                                {
                                    b = RawDataReader.ReadByte(); // read next byte
                                    pos++;   // skip stuff char
                                    packet_array[packet_array_idx] = (byte)(b ^ 0x20); // get real value (XOR 0x20)
                                    packet_array_idx++;
                                }
                                else // regular char
                                {
                                    packet_array[packet_array_idx] = b;
                                    packet_array_idx++;
                                }
                                break;

                            default:
                                pack_state = pack_states.FIND_0X7E;
                                packet_array_idx = 0;
                                break;
                        }
                        // Advance our position variable.
                        pos++;
                    }
                }

                RawDataReader.Close();

            }

        }

        private void check_seq_number_gaps(FileStream fs)
        {
            try
            {

                if (fs != null)
                {
                    String SortedFileName = fs.Name + ".sorted.csv";
                    if (System.IO.File.Exists(SortedFileName))
                    {
                        log_status("Checking for Seq gaps file:" + fs.Name + ".....");
                        string[] lines = File.ReadAllLines(SortedFileName);
                        UInt32 i;
                        UInt32 Seq = 0;
                        UInt32 ShadowSeq = 0;
                        String[] DataStr;
                        UInt32 Gaps = 0;
                        for (i = 0; i < lines.Length; i++)
                        {
                            DataStr = lines[i].Split(',');
                            Seq = Convert.ToUInt32(DataStr[0]);
                            if ((int)(Seq - ShadowSeq) > 1 && ShadowSeq != 0)
                            {
                                log_status("Seq, Number Error!:" + " Seq:" + Seq.ToString() + " Shadow:" + ShadowSeq.ToString());
                                DatalossCount[1]++;
                                Gaps++;
                            }
                            ShadowSeq = Seq;
                        }
                        log_status("File " + fs.Name + ": Gaps found: " + Gaps.ToString());
                    }
                }

            }
            catch (System.IO.IOException e)
            {
                log_status("Error Message = " + e.Message);
            }
        }



        public void Parse_Thread(String FileToParse)
        {
            int i = 0;
            FileTimeStamp = DateTime.Now.ToString("yyyy-MMMM-dd_HH-mm-ss-tt");
            log_status("Binary Parsing Started ... "+ FileToParse);
            DatalossCount[0] = 0;
            DatalossCount[1] = 0;
            log_FCS("0");
            log_SEQ("0");
            log_Parse(0);
            parse_binary_file(FileToParse);
            log_status("Binary Parsing Ended ... " + FileToParse);

            // close all open files
            for (i = 0; i < MaxStreamNumber; i++)
            {
                if (FilesHandle[i] != null)
                {
                    //sort files
                    {
                        log_status("Sorting Started ..");
                        sort_files(FilesHandle[i]);
                        sort_files(SeqFilesHandle[i]);
                        check_seq_number_gaps(SeqFilesHandle[i]);
                        log_status("Sorting Ended ...");
                    }

                    FilesHandle[i].Close();
                    SeqFilesHandle[i].Close();

                    SeqFilesHandle[i] = null;
                    FilesHandle[i] = null;

                }

            }

            log_FCS(DatalossCount[0].ToString());
            log_SEQ(DatalossCount[1].ToString());

        }



        void log_Parse(int val)
        {
            Console.Write("PARSER: " + val.ToString() + " % Complete\r");
            if(val==100)
                Console.Write("\n");

        }


        void log_FCS(String text)
        {
            Console.WriteLine("FCS: " + text);

        }

        void log_SEQ(String text)
        {
            Console.WriteLine("SEQ: " + text);
        }


        void log_data(String DataStr)
        {
            Console.WriteLine("DATA: " + DataStr);
        }

        void log_status(String StatusStr)
        {
            Console.WriteLine("STATUS: " + StatusStr);
        }


        private void UserInitialization(String BinFileDirName, Int32 MaxPacketSize, Int32 MaxStreamNum)
        {
            int i;

            //get data from app properties
            MaxStreamNumber = MaxStreamNum;
            DownloadPath = BinFileDirName.Replace(".bin", "");

            pack_state = pack_states.FIND_0X7E;  // start looking for 0x7e
            packet_array = new byte[MaxPacketSize]; // allocate 400 bytes


            FileNames = new String[MaxStreamNumber]; // one file name for each string
            FilesHandle = new FileStream[MaxStreamNumber];
            SeqFilesHandle = new FileStream[MaxStreamNumber];

            for (i = 0; i < MaxStreamNumber; i++)
            {
                FileNames[i] = i.ToString() + "_";
                FilesHandle[i] = null;
                SeqFilesHandle[i] = null;
            }

            RawFileHandle = null;

            // create directory
            if (!System.IO.Directory.Exists(DownloadPath))
            {
                System.IO.Directory.CreateDirectory(DownloadPath);
                log_status("Directory:" + DownloadPath + " Was Created!");
            }
        }


        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                System.Console.WriteLine("Please enter a command in the following format:");
                System.Console.WriteLine("ArtikBioFlashFileParser <Input Files Directory>,[Max Packet Size], [Max Stream Number]");
                System.Console.WriteLine("<Input Files Directory> --> Mandatory Paramater, specifies user bin files directory");
                System.Console.WriteLine("[Max Packet Size]       --> Optional Paramater, Must be greater equal to 400");
                System.Console.WriteLine("[Max Stream Number]     --> Optional Paramater, Must be less than 256");
                System.Console.WriteLine("Example Short Form: ArtikBioFlashFileParser C:\\ArtikBio_Download");
                System.Console.WriteLine("Example Long  Form: ArtikBioFlashFileParser C:\\ArtikBio_Download,400,255");

                return;
            }

            String[] BinFiles = null; 
            Int32 MaxPacketSize = 400; //default values
            Int32 MaxStreamNum  = 255;
            Program p = new Program();

            // Test if input arguments were supplied:
            string[] values = args[0].Split(',');         

            // change defult values to user values
            if(values.Length >1)
            {
                if (values[1] != "" && values[1] != null)
                {
                    Int32 temp = Convert.ToInt32(values[1]);
                    if (temp > 400)
                        MaxPacketSize = temp;
                }
            }

            if (values.Length > 2)
            {
                if (values[2] != "" && values[2] != null)
                {
                    Int32 temp = Convert.ToInt32(values[2]);
                    if (temp < 256)
                        MaxStreamNum = temp;
                }
            }

            // get bin file list
            BinFiles = Directory.GetFiles(values[0], "*.bin");

            p.log_status("Found " + BinFiles.Length.ToString() + " Bin Files .....\r\n");

            // start parsing all the bin files
            foreach (string BinFile in BinFiles)
            {
                p.log_status("--------------------------------------------- Started Parsing file:" + BinFile + "--------------------------------------");
                p.UserInitialization(BinFile,MaxPacketSize,MaxStreamNum);
                p.Parse_Thread(BinFile);
                p.log_status("---------------------------------------------- Endeed Parsing file:" + BinFile + "--------------------------------------\r\n");

            }

            p.log_status("Parsed " + BinFiles.Length.ToString() + " Bin Files .....\r\n");

        }
    }
}
